/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;


import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Integer.parseInt;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Book;
import persistence.BookDAO;
/** 
 *
 * @author usuario
 */

public class BookController extends BaseController {

    private static final Logger LOG = Logger.getLogger(BookController.class.getName());
    private BookDAO bookDAO;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public void index() {     
        //objeto persistencia
        bookDAO = new BookDAO();
        ArrayList<Book> books = null;

        //leer datos de la persistencia
        synchronized (bookDAO) {
            books = bookDAO.getAll();
        }
        request.setAttribute("books", books);
        String name = "index";
        LOG.info("En BookController->" + name);
//        LOG.info("Studies->" + studies.size());
        dispatch("/WEB-INF/view/book/index.jsp");
    }
    public void paginate(String pageString) {
        LOG.info("idString");
        int page = parseInt(pageString);
        //objeto persistencia
        bookDAO = new BookDAO();
        ArrayList<Book> books = null;

        //leer datos de la persistencia
        synchronized (bookDAO) {
            books = bookDAO.getAllPaginate(page);
        }
        request.setAttribute("books", books);
        String name = "index";
        LOG.info("En BookController->" + name);
//        LOG.info("Studies->" + studies.size());
        dispatch("/WEB-INF/view/book/index.jsp");
    }
    public void view(String idString) throws SQLException {
        HttpSession sesion = request.getSession();
        LOG.info("idString");
        long id = toId(idString);
        //objeto persistencia
        bookDAO = new BookDAO();
        Book book = new Book();

        //leer datos de la persistencia
        synchronized (bookDAO) {
            book = bookDAO.getBook(id);
        }
        sesion.setAttribute("book", book);
        request.setAttribute("book", book);
        String name = "index";
        LOG.info("En BookController->" + name);
//        LOG.info("Studies->" + studies.size());
        dispatch("/WEB-INF/view/book/show.jsp");
    }
    public void last() throws SQLException {
        HttpSession sesion = (HttpSession) request.getSession();
        Book book = (Book) sesion.getAttribute("book");
        request.setAttribute("book", book);
        dispatch("/WEB-INF/view/book/show.jsp");
    }

    public void create() {
        dispatch("/WEB-INF/view/book/create.jsp");
    }

    public void store() throws IOException {

        //objeto persistencia
        bookDAO = new BookDAO();
        LOG.info("crear DAO");
        //crear objeto del formulario
        Book book = loadFromRequest();
        
        synchronized (bookDAO) {
            bookDAO.insert(book);
        }
        redirect(contextPath + "/book/index");
    }

    public void edit(String idString) throws SQLException {
        LOG.info("idString");
        long id = toId(idString);
        BookDAO  bookDAO = new BookDAO();
        Book book = bookDAO.getBook(id);
        request.setAttribute("module", book);
        dispatch("/WEB-INF/view/module/edit.jsp");

    }

    public void update() throws IOException, SQLException {
        BookDAO  bookDAO = new BookDAO();
        Book book = loadFromRequest();
        bookDAO.update(book);
        response.sendRedirect(contextPath + "/book");
        return;
    }

    public void delete(String idString) throws IOException, SQLException 
    {
        LOG.info("BORRANDO " + idString);
        long id = toId(idString);
        
        BookDAO  bookDAO = new BookDAO();
        bookDAO.delete(id);
        redirect(contextPath + "/book");

    }
    
    private Book loadFromRequest()
    {
        Book book = new Book();
        LOG.info("Crear modelo");
        book.setId(parseInt(request.getParameter("id")));
        book.setTitle(request.getParameter("title"));
        book.setPages(parseInt(request.getParameter("pages")));
        book.setYear(parseInt(request.getParameter("year")));
        LOG.info("Datos cargados");
        return book;
    }
}

