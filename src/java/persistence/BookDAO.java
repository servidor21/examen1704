/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Book;

/**
 *
 * @page alumno
 */
public class BookDAO {
    
    private static final Logger LOG = Logger.getLogger(BookDAO.class.getName());
    private int size_page = 30;
    private int registros;

    public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://127.0.0.1/examen1703";
//    public static final String DB_URL = "jdbc:mysql://10.2.25.35/examen1702";
    public static final String DB_USER = "root";
    public static final String DB_PASS = "";

    protected Connection connection;

    public BookDAO() {
//        Class.forName("com.mysql.jdbc.Driver");
//        this.connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
    }

    public void connect() {
        try {
            Class.forName(DB_DRIVER);
            connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
            LOG.log(Level.INFO, "BBDD conectada");
        } catch (ClassNotFoundException ex) {
            LOG.log(Level.INFO, "error conexión BBDD");
            LOG.log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            LOG.log(Level.INFO, "error conexión BBDD");
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    public void disconnect() {
        try {
            connection.close();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Book> getAll() {
        PreparedStatement stmt = null;
        ArrayList<Book> books = null;
        try {
            this.connect();
            stmt = connection.prepareStatement("select * from books");
            ResultSet rs = stmt.executeQuery();
            books = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Book book = new Book();
                book.setId(rs.getInt("id"));
                book.setPages(rs.getInt("pages"));
                book.setTitle(rs.getString("title"));
                book.setYear(rs.getInt("year"));

                books.add(book);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return books;
    }

    
    
    public ArrayList<Book> getAllPaginate(int page) {
        PreparedStatement stmt = null;
        ArrayList<Book> books = null;
        int inicio = (page - 1) * this.size_page;

        try {
            this.connect();
            stmt = connection.prepareStatement("select * from books LIMIT "+inicio+","+this.size_page);
            ResultSet rs = stmt.executeQuery();
            books = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Book book = new Book();
                book.setId(rs.getInt("id"));
                book.setTitle(rs.getString("title"));
                book.setPages(rs.getInt("pages"));
                book.setYear(rs.getInt("year"));

                books.add(book);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            Logger.getLogger(BookDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return books;
    }
    
    
    
    public ArrayList<Book> getPages(int page) {
        int size = 15;
        PreparedStatement stmt = null;
        ArrayList<Book> books = null;
        try {
            this.connect();
            stmt = connection.prepareStatement("select * from books limit ? offset ? ");
            stmt.setInt(1, size);
            stmt.setInt(2, (page - 1) * size);

            ResultSet rs = stmt.executeQuery();
            books = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Book book = new Book();
                book.setId(rs.getInt("id"));
                book.setPages(rs.getInt("pages"));
                book.setTitle(rs.getString("title"));
                book.setYear(rs.getInt("year"));

                books.add(book);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return books;
    }

    public Book get(int id) {

        PreparedStatement stmt = null;
        Book book = null;
        try {
            this.connect();
            stmt = connection.prepareStatement("select * from books where id = ? ");
            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();
            rs.next();

            book = new Book();

            book.setId(rs.getInt("id"));
            book.setPages(rs.getInt("pages"));
            book.setTitle(rs.getString("title"));
            book.setYear(rs.getInt("year"));

            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return book;
    }

    
    
    
    public void delete(long id) throws SQLException {
        PreparedStatement stmt = null;
        this.connect();
        stmt = connection.prepareStatement(
                "DELETE FROM books"
                + " WHERE id = ?"
        );
        stmt.setLong(1, id);
        stmt.execute();
        this.disconnect();
    }
    
    
    
    
    public Book getBook(long id) throws SQLException {
        LOG.info("get(id)");
        Book book = new Book();
        this.connect();
        PreparedStatement stmt = connection.prepareStatement(
                "SELECT * FROM books"
                + " WHERE id = ?"
        );
        stmt.setLong(1, id);
        ResultSet rs = stmt.executeQuery();
        LOG.info("consulta hecha book");
        if (rs.next()) {
            LOG.info("Datos ...");
            book.setId(rs.getInt("id"));
            book.setTitle(rs.getString("title"));
            book.setPages(rs.getInt("pages"));
            book.setYear(rs.getInt("year"));
            LOG.info("Datos cargados");
        } else {
            LOG.log(Level.INFO, "No hay datos para el id {0}", id);
        }
        
        
        this.disconnect();
        return book;
    }
    
    
    
    public void insert(Book book) {
        PreparedStatement stmt = null;
        try {
            this.connect();
            stmt = connection.prepareStatement("INSERT INTO books(pages, title, year) VALUES(?, ? ,?)");
            stmt.setInt(1, book.getPages());
            stmt.setString(2, book.getTitle());
            stmt.setInt (3, book.getYear());
            
            
            stmt.execute();
            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return;        
    }

    
    public void update(Book book) throws SQLException {
        this.connect();
        PreparedStatement stmt = connection.prepareStatement(
                "UPDATE books SET "
                        + "title = ?, "
                        + "pages = ?, "
                        + "year = ?, "
                       
                        + " WHERE id = ? "
        );
        stmt.setString(1, book.getTitle());
        stmt.setInt(2, book.getPages());
        stmt.setInt(3, book.getYear());
       
        stmt.setLong(4, book.getId());
        LOG.info("id : " + book.getId());
        stmt.execute();
        this.disconnect();
        return;
    }
    
}
