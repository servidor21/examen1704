<%-- 
    Document   : index.jsp
    Created on : 09-ene-2017, 19:18:05
    Author     : alumno
--%>

<%@page import="model.Book"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="books" class="java.util.ArrayList" scope="request"/>  


<!DOCTYPE html>
<%@include file="/WEB-INF/view/header.jsp" %>

<div id="content">
    <h1>Lista de Books </h1>
    <p>
        <a href="<%= request.getContextPath()%>/book/create">Nuevo book</a>
    </p>
    <div style="padding: 1em;">
        P�ginas: <a href="<%= request.getContextPath()%>/book/paginate/1" style="margin: 0.1em">1</a>
        <a href="<%= request.getContextPath()%>/book/paginate/2" style="margin: 0.1em">2</a>
        <a href="<%= request.getContextPath()%>/book/paginate/3" style="margin: 0.1em">3</a>
        <a href="<%= request.getContextPath()%>/book/paginate/4" style="margin: 0.1em">4</a>
    </div>
    <table>
        <tr>
            <th>Id</th>
            <th>Titulo</th>
            <th>Paginas</th>
            <th>A�o</th>
            <th>Acci�n</th>
        </tr>
        <%        Iterator<model.Book> iterator = books.iterator();
            while (iterator.hasNext()) {
                Book book = iterator.next();%>
        <tr>
            <td><%= book.getId()%></td>
            <td><%= book.getTitle()%></td>
            <td><%= book.getPages()%></td>
            <td><%= book.getYear()%></td>
            <td> 
                <a href="<%= request.getContextPath() + "/book/delete/" + book.getId()%>"> Borrar</a>
                <a href="<%= request.getContextPath() + "/book/edit/" + book.getId()%>"> Editar</a>
                <a href="<%= request.getContextPath() + "/book/view/" + book.getId()%>"> Ver</a>
            </td>
        </tr>
        <%
            }
        %>          </table>
        
        <p>
        <a href="<%= request.getContextPath()%>/book/create">Nuevo book</a>
    </p>
    <div style="padding: 1em;">
        P�ginas: <a href="<%= request.getContextPath()%>/book/paginate/1" style="margin: 0.1em">1</a>
        <a href="<%= request.getContextPath()%>/book/paginate/2" style="margin: 0.1em">2</a>
        <a href="<%= request.getContextPath()%>/book/paginate/3" style="margin: 0.1em">3</a>
        <a href="<%= request.getContextPath()%>/book/paginate/4" style="margin: 0.1em">4</a>
    </div>

</div>
<%@include file="/WEB-INF/view/footer.jsp" %>
