<%-- 
    Document   : index.jsp
    Created on : 09-ene-2017, 19:18:05
    Author     : alumno
--%>

<%@page import="model.Book"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="book" class="model.Book" scope="request"/>  


<!DOCTYPE html>
<%@include file="/WEB-INF/view/header.jsp" %>

<div id="content">
    <h1> Book: <%= book.getTitle()%> </h1>
    <p><b>Id:</b> <%= book.getId()%></p>
    <p><b>P�ginas:</b> <%= book.getPages()%></p>
    <p><b>A�o:</b> <%= book.getYear()%></p>
</div>
<%@include file="/WEB-INF/view/footer.jsp" %>
